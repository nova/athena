/*
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

/**

@page TrigT2CaloTau_page 
@author Carlos Osuna
@author Stefania Xella
@author M. Pilar Casado
@author Olga Igonkina

@section TrigT2CaloTau_TrigT2CaloTauOverview Overview
This package is in charge of the calorimeter reconstruction in
the trigger LVL2 for taus. It builds a set of shower shape variables
to discriminate jet and taus.
Deposited energy is available in 3 different window sizes for all sampling.



*/
