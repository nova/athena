#include "../GenEventValidityChecker.h"
#include "../GenEventVertexPositioner.h"
#include "../VertexBeamCondPositioner.h"
#include "../LongBeamspotVertexPositioner.h"
#include "../CrabKissingVertexPositioner.h"
#include "../VertexPositionFromFile.h"
#include "../GenEventBeamEffectBooster.h"
#include "../GenEventRotator.h"
#include "../BeamEffectsAlg.h"
DECLARE_COMPONENT( Simulation::GenEventValidityChecker )
DECLARE_COMPONENT( Simulation::GenEventVertexPositioner )
DECLARE_COMPONENT( Simulation::VertexBeamCondPositioner )
DECLARE_COMPONENT( Simulation::LongBeamspotVertexPositioner )
DECLARE_COMPONENT( Simulation::CrabKissingVertexPositioner )
DECLARE_COMPONENT( Simulation::VertexPositionFromFile )
DECLARE_COMPONENT( Simulation::GenEventBeamEffectBooster )
DECLARE_COMPONENT( Simulation::GenEventRotator )
DECLARE_COMPONENT( Simulation::BeamEffectsAlg )

