#
# File specifying the location of MCUtils to use.
#

set( MCUTILS_VERSION 1.3.2 )
set( MCUTILS_ROOT
   ${LCG_RELEASE_DIR}/MCGenerators/mcutils/${MCUTILS_VERSION}/${LCG_PLATFORM} )
