/*
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

/**
@page Herwigpp_i_page Herwigpp_i
@author Andy.Buckley@cern.ch,  judith.katzy@cern.ch

@section Herwigpp_i_The interface between Herwig++ and Athena

Herwig++ is a general-purpose generator written in c++ with many 
modifications and improvements on fortran Herwig. 
More information and the source code of the generator are available on the 
page:
http://projects.hepforge.org/herwig/

Athena interface for Herwig++ was written by Andy Buckley and is available 
since rel.14.




*/
